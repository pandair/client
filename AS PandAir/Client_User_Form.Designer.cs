﻿namespace AS_PandAir
{
    partial class Client_User_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Client_User_Form));
            this.BuyTicketsPanel = new System.Windows.Forms.Panel();
            this.TicketBookingPanel = new System.Windows.Forms.Panel();
            this.BackToMenu = new System.Windows.Forms.Button();
            this.BabiesGroup = new System.Windows.Forms.GroupBox();
            this.NoBabies = new System.Windows.Forms.RadioButton();
            this.OneBaby = new System.Windows.Forms.RadioButton();
            this.TwoBabies = new System.Windows.Forms.RadioButton();
            this.ThreeBabies = new System.Windows.Forms.RadioButton();
            this.ChildrenGroup = new System.Windows.Forms.GroupBox();
            this.NoChildren = new System.Windows.Forms.RadioButton();
            this.OneChild = new System.Windows.Forms.RadioButton();
            this.TwoChildren = new System.Windows.Forms.RadioButton();
            this.ThreeChildren = new System.Windows.Forms.RadioButton();
            this.FourChildren = new System.Windows.Forms.RadioButton();
            this.FiveChildren = new System.Windows.Forms.RadioButton();
            this.AdultsGroup = new System.Windows.Forms.GroupBox();
            this.OneAdult = new System.Windows.Forms.RadioButton();
            this.TwoAdult = new System.Windows.Forms.RadioButton();
            this.ThreeAdult = new System.Windows.Forms.RadioButton();
            this.FourAdult = new System.Windows.Forms.RadioButton();
            this.FiveAdult = new System.Windows.Forms.RadioButton();
            this.SixAdult = new System.Windows.Forms.RadioButton();
            this.FindFlights = new System.Windows.Forms.Button();
            this.InfoLabel = new System.Windows.Forms.Label();
            this.Information2 = new System.Windows.Forms.Label();
            this.Information1 = new System.Windows.Forms.Label();
            this.ClassOfServiceChoose = new System.Windows.Forms.ComboBox();
            this.ClassOfService = new System.Windows.Forms.Label();
            this.DateBack = new System.Windows.Forms.Label();
            this.DateThere = new System.Windows.Forms.Label();
            this.DateBackChoose = new System.Windows.Forms.DateTimePicker();
            this.CityFrom = new System.Windows.Forms.ComboBox();
            this.CityTo = new System.Windows.Forms.ComboBox();
            this.To = new System.Windows.Forms.Label();
            this.From = new System.Windows.Forms.Label();
            this.OneDirection = new System.Windows.Forms.Button();
            this.ThereAndBack = new System.Windows.Forms.Button();
            this.DateThereChoose = new System.Windows.Forms.DateTimePicker();
            this.Head = new System.Windows.Forms.Label();
            this.TicketBookingPanel.SuspendLayout();
            this.BabiesGroup.SuspendLayout();
            this.ChildrenGroup.SuspendLayout();
            this.AdultsGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // BuyTicketsPanel
            // 
            this.BuyTicketsPanel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BuyTicketsPanel.BackgroundImage")));
            this.BuyTicketsPanel.Location = new System.Drawing.Point(395, 0);
            this.BuyTicketsPanel.Name = "BuyTicketsPanel";
            this.BuyTicketsPanel.Size = new System.Drawing.Size(575, 681);
            this.BuyTicketsPanel.TabIndex = 0;
            // 
            // TicketBookingPanel
            // 
            this.TicketBookingPanel.Controls.Add(this.BackToMenu);
            this.TicketBookingPanel.Controls.Add(this.BabiesGroup);
            this.TicketBookingPanel.Controls.Add(this.ChildrenGroup);
            this.TicketBookingPanel.Controls.Add(this.AdultsGroup);
            this.TicketBookingPanel.Controls.Add(this.FindFlights);
            this.TicketBookingPanel.Controls.Add(this.InfoLabel);
            this.TicketBookingPanel.Controls.Add(this.Information2);
            this.TicketBookingPanel.Controls.Add(this.Information1);
            this.TicketBookingPanel.Controls.Add(this.ClassOfServiceChoose);
            this.TicketBookingPanel.Controls.Add(this.ClassOfService);
            this.TicketBookingPanel.Controls.Add(this.DateBack);
            this.TicketBookingPanel.Controls.Add(this.DateThere);
            this.TicketBookingPanel.Controls.Add(this.DateBackChoose);
            this.TicketBookingPanel.Controls.Add(this.CityFrom);
            this.TicketBookingPanel.Controls.Add(this.CityTo);
            this.TicketBookingPanel.Controls.Add(this.To);
            this.TicketBookingPanel.Controls.Add(this.From);
            this.TicketBookingPanel.Controls.Add(this.OneDirection);
            this.TicketBookingPanel.Controls.Add(this.ThereAndBack);
            this.TicketBookingPanel.Controls.Add(this.DateThereChoose);
            this.TicketBookingPanel.Controls.Add(this.Head);
            this.TicketBookingPanel.Location = new System.Drawing.Point(0, 0);
            this.TicketBookingPanel.Name = "TicketBookingPanel";
            this.TicketBookingPanel.Size = new System.Drawing.Size(422, 681);
            this.TicketBookingPanel.TabIndex = 1;
            // 
            // BackToMenu
            // 
            this.BackToMenu.Location = new System.Drawing.Point(130, 587);
            this.BackToMenu.Name = "BackToMenu";
            this.BackToMenu.Size = new System.Drawing.Size(147, 47);
            this.BackToMenu.TabIndex = 53;
            this.BackToMenu.Text = "Вернуться в меню";
            this.BackToMenu.UseVisualStyleBackColor = true;
            this.BackToMenu.Click += new System.EventHandler(this.BackToMenu_Click);
            // 
            // BabiesGroup
            // 
            this.BabiesGroup.Controls.Add(this.NoBabies);
            this.BabiesGroup.Controls.Add(this.OneBaby);
            this.BabiesGroup.Controls.Add(this.TwoBabies);
            this.BabiesGroup.Controls.Add(this.ThreeBabies);
            this.BabiesGroup.Location = new System.Drawing.Point(42, 402);
            this.BabiesGroup.Name = "BabiesGroup";
            this.BabiesGroup.Size = new System.Drawing.Size(129, 115);
            this.BabiesGroup.TabIndex = 39;
            this.BabiesGroup.TabStop = false;
            this.BabiesGroup.Text = "Младенцы (до 2 лет):";
            // 
            // NoBabies
            // 
            this.NoBabies.AutoSize = true;
            this.NoBabies.Checked = true;
            this.NoBabies.Location = new System.Drawing.Point(6, 19);
            this.NoBabies.Name = "NoBabies";
            this.NoBabies.Size = new System.Drawing.Size(31, 17);
            this.NoBabies.TabIndex = 40;
            this.NoBabies.TabStop = true;
            this.NoBabies.Text = "0";
            this.NoBabies.UseVisualStyleBackColor = true;
            // 
            // OneBaby
            // 
            this.OneBaby.AutoSize = true;
            this.OneBaby.Location = new System.Drawing.Point(6, 42);
            this.OneBaby.Name = "OneBaby";
            this.OneBaby.Size = new System.Drawing.Size(31, 17);
            this.OneBaby.TabIndex = 41;
            this.OneBaby.Text = "1";
            this.OneBaby.UseVisualStyleBackColor = true;
            this.OneBaby.CheckedChanged += new System.EventHandler(this.OneBaby_CheckedChanged);
            // 
            // TwoBabies
            // 
            this.TwoBabies.AutoSize = true;
            this.TwoBabies.Location = new System.Drawing.Point(6, 65);
            this.TwoBabies.Name = "TwoBabies";
            this.TwoBabies.Size = new System.Drawing.Size(31, 17);
            this.TwoBabies.TabIndex = 42;
            this.TwoBabies.Text = "2";
            this.TwoBabies.UseVisualStyleBackColor = true;
            this.TwoBabies.CheckedChanged += new System.EventHandler(this.TwoBabies_CheckedChanged);
            // 
            // ThreeBabies
            // 
            this.ThreeBabies.AutoSize = true;
            this.ThreeBabies.Location = new System.Drawing.Point(6, 88);
            this.ThreeBabies.Name = "ThreeBabies";
            this.ThreeBabies.Size = new System.Drawing.Size(31, 17);
            this.ThreeBabies.TabIndex = 43;
            this.ThreeBabies.Text = "3";
            this.ThreeBabies.UseVisualStyleBackColor = true;
            this.ThreeBabies.CheckedChanged += new System.EventHandler(this.ThreeBabies_CheckedChanged);
            // 
            // ChildrenGroup
            // 
            this.ChildrenGroup.Controls.Add(this.NoChildren);
            this.ChildrenGroup.Controls.Add(this.OneChild);
            this.ChildrenGroup.Controls.Add(this.TwoChildren);
            this.ChildrenGroup.Controls.Add(this.ThreeChildren);
            this.ChildrenGroup.Controls.Add(this.FourChildren);
            this.ChildrenGroup.Controls.Add(this.FiveChildren);
            this.ChildrenGroup.Location = new System.Drawing.Point(237, 237);
            this.ChildrenGroup.Name = "ChildrenGroup";
            this.ChildrenGroup.Size = new System.Drawing.Size(130, 159);
            this.ChildrenGroup.TabIndex = 52;
            this.ChildrenGroup.TabStop = false;
            this.ChildrenGroup.Text = "Дети (от 2 до 12 лет):";
            // 
            // NoChildren
            // 
            this.NoChildren.AutoSize = true;
            this.NoChildren.Checked = true;
            this.NoChildren.Location = new System.Drawing.Point(6, 19);
            this.NoChildren.Name = "NoChildren";
            this.NoChildren.Size = new System.Drawing.Size(31, 17);
            this.NoChildren.TabIndex = 33;
            this.NoChildren.TabStop = true;
            this.NoChildren.Text = "0";
            this.NoChildren.UseVisualStyleBackColor = true;
            // 
            // OneChild
            // 
            this.OneChild.AutoSize = true;
            this.OneChild.Location = new System.Drawing.Point(6, 42);
            this.OneChild.Name = "OneChild";
            this.OneChild.Size = new System.Drawing.Size(31, 17);
            this.OneChild.TabIndex = 34;
            this.OneChild.Text = "1";
            this.OneChild.UseVisualStyleBackColor = true;
            this.OneChild.CheckedChanged += new System.EventHandler(this.OneChild_CheckedChanged);
            // 
            // TwoChildren
            // 
            this.TwoChildren.AutoSize = true;
            this.TwoChildren.Location = new System.Drawing.Point(6, 65);
            this.TwoChildren.Name = "TwoChildren";
            this.TwoChildren.Size = new System.Drawing.Size(31, 17);
            this.TwoChildren.TabIndex = 35;
            this.TwoChildren.Text = "2";
            this.TwoChildren.UseVisualStyleBackColor = true;
            this.TwoChildren.CheckedChanged += new System.EventHandler(this.TwoChildren_CheckedChanged);
            // 
            // ThreeChildren
            // 
            this.ThreeChildren.AutoSize = true;
            this.ThreeChildren.Location = new System.Drawing.Point(6, 88);
            this.ThreeChildren.Name = "ThreeChildren";
            this.ThreeChildren.Size = new System.Drawing.Size(31, 17);
            this.ThreeChildren.TabIndex = 36;
            this.ThreeChildren.Text = "3";
            this.ThreeChildren.UseVisualStyleBackColor = true;
            this.ThreeChildren.CheckedChanged += new System.EventHandler(this.ThreeChildren_CheckedChanged);
            // 
            // FourChildren
            // 
            this.FourChildren.AutoSize = true;
            this.FourChildren.Location = new System.Drawing.Point(6, 111);
            this.FourChildren.Name = "FourChildren";
            this.FourChildren.Size = new System.Drawing.Size(31, 17);
            this.FourChildren.TabIndex = 37;
            this.FourChildren.Text = "4";
            this.FourChildren.UseVisualStyleBackColor = true;
            this.FourChildren.CheckedChanged += new System.EventHandler(this.FourChildren_CheckedChanged);
            // 
            // FiveChildren
            // 
            this.FiveChildren.AutoSize = true;
            this.FiveChildren.Location = new System.Drawing.Point(6, 134);
            this.FiveChildren.Name = "FiveChildren";
            this.FiveChildren.Size = new System.Drawing.Size(31, 17);
            this.FiveChildren.TabIndex = 38;
            this.FiveChildren.Text = "5";
            this.FiveChildren.UseVisualStyleBackColor = true;
            this.FiveChildren.CheckedChanged += new System.EventHandler(this.FiveChildren_CheckedChanged);
            // 
            // AdultsGroup
            // 
            this.AdultsGroup.Controls.Add(this.OneAdult);
            this.AdultsGroup.Controls.Add(this.TwoAdult);
            this.AdultsGroup.Controls.Add(this.ThreeAdult);
            this.AdultsGroup.Controls.Add(this.FourAdult);
            this.AdultsGroup.Controls.Add(this.FiveAdult);
            this.AdultsGroup.Controls.Add(this.SixAdult);
            this.AdultsGroup.Location = new System.Drawing.Point(39, 237);
            this.AdultsGroup.Name = "AdultsGroup";
            this.AdultsGroup.Size = new System.Drawing.Size(132, 159);
            this.AdultsGroup.TabIndex = 51;
            this.AdultsGroup.TabStop = false;
            this.AdultsGroup.Text = "Взрослые (от 12 лет):";
            // 
            // OneAdult
            // 
            this.OneAdult.AutoSize = true;
            this.OneAdult.Checked = true;
            this.OneAdult.Location = new System.Drawing.Point(9, 19);
            this.OneAdult.Name = "OneAdult";
            this.OneAdult.Size = new System.Drawing.Size(31, 17);
            this.OneAdult.TabIndex = 25;
            this.OneAdult.TabStop = true;
            this.OneAdult.Text = "1";
            this.OneAdult.UseVisualStyleBackColor = true;
            this.OneAdult.CheckedChanged += new System.EventHandler(this.OneAdult_CheckedChanged);
            // 
            // TwoAdult
            // 
            this.TwoAdult.AutoSize = true;
            this.TwoAdult.Location = new System.Drawing.Point(9, 42);
            this.TwoAdult.Name = "TwoAdult";
            this.TwoAdult.Size = new System.Drawing.Size(31, 17);
            this.TwoAdult.TabIndex = 26;
            this.TwoAdult.Text = "2";
            this.TwoAdult.UseVisualStyleBackColor = true;
            this.TwoAdult.CheckedChanged += new System.EventHandler(this.TwoAdults_CheckedChanged);
            // 
            // ThreeAdult
            // 
            this.ThreeAdult.AutoSize = true;
            this.ThreeAdult.Location = new System.Drawing.Point(9, 65);
            this.ThreeAdult.Name = "ThreeAdult";
            this.ThreeAdult.Size = new System.Drawing.Size(31, 17);
            this.ThreeAdult.TabIndex = 27;
            this.ThreeAdult.Text = "3";
            this.ThreeAdult.UseVisualStyleBackColor = true;
            this.ThreeAdult.CheckedChanged += new System.EventHandler(this.ThreeAdults_CheckedChanged);
            // 
            // FourAdult
            // 
            this.FourAdult.AutoSize = true;
            this.FourAdult.Location = new System.Drawing.Point(9, 88);
            this.FourAdult.Name = "FourAdult";
            this.FourAdult.Size = new System.Drawing.Size(31, 17);
            this.FourAdult.TabIndex = 29;
            this.FourAdult.Text = "4";
            this.FourAdult.UseVisualStyleBackColor = true;
            this.FourAdult.CheckedChanged += new System.EventHandler(this.FourAdult_CheckedChanged);
            // 
            // FiveAdult
            // 
            this.FiveAdult.AutoSize = true;
            this.FiveAdult.Location = new System.Drawing.Point(9, 115);
            this.FiveAdult.Name = "FiveAdult";
            this.FiveAdult.Size = new System.Drawing.Size(31, 17);
            this.FiveAdult.TabIndex = 30;
            this.FiveAdult.Text = "5";
            this.FiveAdult.UseVisualStyleBackColor = true;
            this.FiveAdult.CheckedChanged += new System.EventHandler(this.FiveAdult_CheckedChanged);
            // 
            // SixAdult
            // 
            this.SixAdult.AutoSize = true;
            this.SixAdult.Location = new System.Drawing.Point(9, 138);
            this.SixAdult.Name = "SixAdult";
            this.SixAdult.Size = new System.Drawing.Size(31, 17);
            this.SixAdult.TabIndex = 31;
            this.SixAdult.Text = "6";
            this.SixAdult.UseVisualStyleBackColor = true;
            this.SixAdult.CheckedChanged += new System.EventHandler(this.SixAdult_CheckedChanged);
            // 
            // FindFlights
            // 
            this.FindFlights.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FindFlights.Location = new System.Drawing.Point(130, 523);
            this.FindFlights.Name = "FindFlights";
            this.FindFlights.Size = new System.Drawing.Size(147, 47);
            this.FindFlights.TabIndex = 50;
            this.FindFlights.Text = "НАЙТИ РЕЙСЫ";
            this.FindFlights.UseVisualStyleBackColor = true;
            // 
            // InfoLabel
            // 
            this.InfoLabel.AutoSize = true;
            this.InfoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.InfoLabel.Location = new System.Drawing.Point(44, 644);
            this.InfoLabel.Name = "InfoLabel";
            this.InfoLabel.Size = new System.Drawing.Size(21, 31);
            this.InfoLabel.TabIndex = 49;
            this.InfoLabel.Text = "i";
            // 
            // Information2
            // 
            this.Information2.AutoSize = true;
            this.Information2.Location = new System.Drawing.Point(71, 659);
            this.Information2.Name = "Information2";
            this.Information2.Size = new System.Drawing.Size(168, 13);
            this.Information2.TabIndex = 48;
            this.Information2.Text = "не может превышать 6 человек";
            // 
            // Information1
            // 
            this.Information1.AutoSize = true;
            this.Information1.Location = new System.Drawing.Point(71, 646);
            this.Information1.Name = "Information1";
            this.Information1.Size = new System.Drawing.Size(279, 13);
            this.Information1.TabIndex = 47;
            this.Information1.Text = "Количество пассажиров при покупке билетов онлайн";
            // 
            // ClassOfServiceChoose
            // 
            this.ClassOfServiceChoose.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ClassOfServiceChoose.FormattingEnabled = true;
            this.ClassOfServiceChoose.Items.AddRange(new object[] {
            "Эконом",
            "Комфорт",
            "Бизнес"});
            this.ClassOfServiceChoose.Location = new System.Drawing.Point(302, 413);
            this.ClassOfServiceChoose.Name = "ClassOfServiceChoose";
            this.ClassOfServiceChoose.Size = new System.Drawing.Size(93, 21);
            this.ClassOfServiceChoose.TabIndex = 46;
            // 
            // ClassOfService
            // 
            this.ClassOfService.AutoSize = true;
            this.ClassOfService.Location = new System.Drawing.Point(179, 421);
            this.ClassOfService.Name = "ClassOfService";
            this.ClassOfService.Size = new System.Drawing.Size(117, 13);
            this.ClassOfService.TabIndex = 44;
            this.ClassOfService.Text = "Класс обслуживания:";
            // 
            // DateBack
            // 
            this.DateBack.AutoSize = true;
            this.DateBack.Location = new System.Drawing.Point(216, 182);
            this.DateBack.Name = "DateBack";
            this.DateBack.Size = new System.Drawing.Size(80, 13);
            this.DateBack.TabIndex = 24;
            this.DateBack.Text = "Дата обратно:";
            // 
            // DateThere
            // 
            this.DateThere.AutoSize = true;
            this.DateThere.Location = new System.Drawing.Point(39, 182);
            this.DateThere.Name = "DateThere";
            this.DateThere.Size = new System.Drawing.Size(61, 13);
            this.DateThere.TabIndex = 23;
            this.DateThere.Text = "Дата туда:";
            // 
            // DateBackChoose
            // 
            this.DateBackChoose.Checked = false;
            this.DateBackChoose.Enabled = false;
            this.DateBackChoose.Location = new System.Drawing.Point(219, 198);
            this.DateBackChoose.MaxDate = new System.DateTime(2100, 12, 31, 0, 0, 0, 0);
            this.DateBackChoose.MinDate = new System.DateTime(2018, 1, 1, 0, 0, 0, 0);
            this.DateBackChoose.Name = "DateBackChoose";
            this.DateBackChoose.Size = new System.Drawing.Size(148, 20);
            this.DateBackChoose.TabIndex = 22;
            // 
            // CityFrom
            // 
            this.CityFrom.FormattingEnabled = true;
            this.CityFrom.Items.AddRange(new object[] {
            "Абакан",
            "Анадырь",
            "Анапа",
            "Архангельск",
            "Астрахань",
            "Барнаул",
            "Белгород",
            "Благовещенск",
            "Владивосток",
            "Волгоград",
            "Воронеж",
            "Геленджик",
            "Горно-Алтайск",
            "Екатеринбург",
            "Иркутск",
            "Казань",
            "Калининград",
            "Кемерово",
            "Комсомольск-на-Амуре",
            "Краснодар",
            "Красноярск",
            "Магадан",
            "Магнитогорск",
            "Махачкала",
            "Минеральные Воды",
            "Москва",
            "Мурманск",
            "Нарьян-Мар",
            "Нерюнгри",
            "Нижневартовск",
            "Нижнекамск",
            "Нижний Новгород",
            "Николаевск-на-Амуре",
            "Новокузнецк",
            "Новосибирск",
            "Новый Уренгой",
            "Ноглики",
            "Норильск",
            "Омск",
            "Оренбург",
            "Остров Итуруп",
            "Оха",
            "Пермь",
            "Петропавловск-Камчатский",
            "Ростов-на-Дону",
            "Салехард",
            "Самара",
            "Санкт-Петербург",
            "Саранск",
            "Саратов",
            "Симферополь",
            "Советская Гавань",
            "Соловецкий",
            "Сочи",
            "Ставрополь",
            "Сургут",
            "Сыктывкар",
            "томск",
            "Тында",
            "Тюмень",
            "Ульяновск",
            "Уфа",
            "Хабаровск",
            "Ханты-Мансийск",
            "Челябинск",
            "Чита",
            "Шахтерск",
            "Южно-Курильск",
            "Южно-Сахалинск",
            "Якутск",
            "Баку (Азербайджан)",
            "Ереван (Армения)",
            "Минск (Беларусь)",
            "Актау (Казахстан)",
            "Актобе (Казахстан)",
            "Алматы (Казахстан)",
            "Астана (Казахстан)",
            "Атырау (Казахстан)",
            "Караганда (Казахстан)",
            "Костанай (Казахстан)",
            "Кызылорда (Казахстан)",
            "Павлодар (Казахстан)",
            "Усть-Каменогорск (Казахстан)",
            "Шымкент (Казахстан)",
            "Бишкек (Киргизия)",
            "Ош (Киргизия)",
            "Кишинев (Молдова)",
            "Душанбе (Таджикистан)",
            "Худжанд (Таджикистан)",
            "Ашхабад (Туркменистан)",
            "Бухара (Узбекистан)",
            "Карши (Узбекистан)",
            "Навои (Узбекистан)",
            "Наманган (Узбекистан)",
            "Самарканд (Узбекистан)",
            "Ташкент (Узбекистан)",
            "Ургенч (Узбекистан)",
            "Фергана (Узбекистан)",
            "Днепропетровск (Украина)",
            "Донецк (Украина)",
            "Киев (Украина)",
            "Одесса (Украина)",
            "Харьков (Украина)",
            "Аделаида (Австралия)",
            "Брисбен (Австралия)",
            "Мельбурн (Австралия)",
            "Перт (Австралия)",
            "Сидней (Австралия)",
            "Дакка (Бангладеш)",
            "Далат (Вьетнам)",
            "Дананг (Вьетнам)",
            "Нячанг (Вьетнам)",
            "Фукуок (Вьетнам)",
            "Ханой (Вьетнам)",
            "Хаошимин (Вьетнам)",
            "Хюэ (Вьетнам)",
            "Тбилиси (Грузия)",
            "Амритсар (Индия)",
            "Ахмедабад (Индия)",
            "Бангалор (Индия)",
            "Бхубанесвар (Индия)",
            "Вадодара (Индия)",
            "Вишакхапатнам (Индия)",
            "Гоа (Индия)",
            "Дели (Индия)",
            "Джайпур (Индия)",
            "Коимбаторе (Индия)",
            "Колката (Индия)",
            "Кочин (Индия)",
            "Мумбай (Индия)",
            "Пуна (Индия)",
            "Тривандрам (Индия)",
            "Удайпур (Индия)",
            "Хайдерабад (Индия)",
            "Чандигарх (Индия)",
            "Ченнай (Индия)",
            "Денпасар Бали (Индонезия)",
            "Джакарта (Индонезия)",
            "Сурабая (Индонезия)",
            "Пномпень (Камбоджа)",
            "Сием Рип (Камбоджа)",
            "Баотоу (Китай)",
            "Вэйфан (Китай)",
            "Вэньчжоу (Китай)",
            "Гаосюн (Китай)",
            "Гонконг (Китай)",
            "Гуанчжоу (Китай)",
            "Гуйлинь (Китай)",
            "Гуйян (Китай)",
            "Далянь (Китай)",
            "Джаймуси (Китай)",
            "Ичан (Китай)",
            "Куньмин (Китай)",
            "Ланчжоу (Китай)",
            "Маньчжурия (Китай)",
            "Нанкин (Китай)",
            "Наньнин (Китай)",
            "Наньчан (Китай)",
            "Нинбо (Китай)",
            "Пекин (Китай)",
            "Санья (Китай)",
            "Синин (Китай)",
            "Синьян (Китай)",
            "Сямынь (Китай)",
            "Тайбэй (Китай)",
            "Тайюань (Китай)",
            "Тяньцзинь (Китай)",
            "Урумчи (Китай)",
            "Ухань (Китай)",
            "Хайкоу (Китай)",
            "Ханчжоу (Китай)",
            "Харбин (Китай)",
            "Хуайань (Китай)",
            "Цзинань (Китай)",
            "Циндао (Китай)",
            "Цицикар (Китай)",
            "Чангжи (Китай)",
            "Чанчунь (Китай)",
            "Чанша (Китай)",
            "Чэнду (Китай)",
            "Шанхай (Китай)",
            "Шаньтоу (Китай)",
            "Шицзячжуан (Китай)",
            "Шэньчжэнь (Китай)",
            "Шэньян (Китай)",
            "Юйлинь (Китай)",
            "Яньань (Китай)",
            "Вьентьян (Лаос)",
            "Куала-Лумпур (Малайзия)",
            "Мале (Мальдивы)",
            "Улан Батор (Монголия)",
            "Мандалай (Мьянма)",
            "Нейпьидо (Мьянма)",
            "Янгон (Мьянма)",
            "Окленд (Новая Зеландия)",
            "Исламабад (Пакистан)",
            "Карачи (Пакистан)",
            "Сингапур (Сингапур)",
            "Бангкок (Таиланд)",
            "Кох Самуи (Таиланд)",
            "Краби (Таиланд)",
            "Лампанг (Таиланд)",
            "Пхукет (Таиланд)",
            "Сукхотаи (Таиланд)",
            "Трат (Таиланд)",
            "Утапао (Таиланд)",
            "Чианг Рай (Таиланд)",
            "Чиангмай (Таиланд)",
            "Чианграй (Таиланд)",
            "Манила (Филиппины)",
            "Себу Мактан (Филиппины)",
            "Коломбо (Шри-Ланка)",
            "Пусан (Ю. Корея)",
            "Сеул (Ю. Корея)",
            "Ульсан (Ю. Корея)",
            "Чеджу (Ю. Корея)",
            "Нагоя (Япония)",
            "Ниигата (Япония)",
            "Обихиро (Япония)",
            "Окинава (Япония)",
            "Осака (Япония)",
            "Саппоро (Япония)",
            "Токио (Япония)",
            "Тояма (Япония)",
            "Фукуока (Япония)",
            "Хакодате (Япония)",
            "Хиросима (Япония)",
            "Пуанта-Кана (Доминиканская респ.)",
            "Гавана (Куба)",
            "Канкун (Мексика)",
            "Мехико (Мексика)",
            "Сайпан (Северные Марианские о-ва)",
            "Альбукерке (США)",
            "Атланта (США)",
            "Балтимор (США)",
            "Баффало (США)",
            "Берлингтон (США)",
            "Бостон (США)",
            "Бурбанк (США)",
            "Вашингтон (США)",
            "Вест-Палм-Бич (США)",
            "Гуам (США)",
            "Дайтона-Бич (США)",
            "Даллас (США)",
            "Денвер (США)",
            "Детройт (США)",
            "Джэксонвилл (США)",
            "Индианаполис (США)",
            "Канзас-Сити (США)",
            "Кливленд (США)",
            "Лас-Вегас (США)",
            "Лонг-Бич (США)",
            "Лос-Анджелес (США)",
            "Майами (США)",
            "Мартас-Винъярд (США)",
            "Мемфис (США)",
            "Миннеаполис (США)",
            "Нантакет (США)",
            "Новый Орлеан (США)",
            "Нью-Йорк (США)",
            "Окленд (США)",
            "Орландо (США)",
            "Остин (США)",
            "Питтсбург (США)",
            "Портленд (США)",
            "Рено-Тахо (США)",
            "Ричмонд (США)",
            "Роли (США)",
            "Рочестер (США)",
            "Саванна (США)",
            "Сакраменто (США)",
            "Сан-Антонио (США)",
            "Сан-Диего (США)",
            "Сан-Франциско (США)",
            "Сан-Хосе (США)",
            "Сарасота-Брадентон (США)",
            "Сент-Луис (США)",
            "Сиракус (США)",
            "Сиэтл (США)",
            "Солт-Лейк-Сити (США)",
            "Тампа (США)",
            "Феникс (США)",
            "Филадельфия (США)",
            "Форт Майерс Пейдж (США)",
            "Форт-Лодердейл (США)",
            "Хартфорд (США)",
            "Хианнис (США)",
            "Хьюстон (США)",
            "Цинциннати (США)",
            "Чарльстон (США)",
            "Чикаго (США)",
            "Шарлотт (США)",
            "Алжир (Алжир)",
            "Аккра (Гана)",
            "Каир (Египет)",
            "Хургада (Египет)",
            "Шарм Эль Шейх (Египет)",
            "Тель-Авив (Израиль)",
            "Эйлат (Израиль)",
            "Тегеран (Иран)",
            "Найроби (Кения)",
            "Бейрут (Ливан)",
            "Касабланка (Марокко)",
            "Марракеш (Марокко)",
            "Дубай (ОАЭ)",
            "Джидда (Саудовская Аравия)",
            "Эр-Рияд (Саудовская Аравия)",
            "Дамаск (Сирия)",
            "Тунис (Тунис)",
            "Вена (Австрия)",
            "Зальцбург (Австрия)",
            "Инсбрук (Австрия)",
            "Тирана (Албания)",
            "Брюссель (Бельгия)",
            "Бургас (Болгария)",
            "Варна (Болгария)",
            "София (Болгария)",
            "Баня-Лука (Босния и Герцеговина)",
            "Сараево (Босния и Герцеговина)",
            "Абердин (Великобритания)",
            "Белфаст (Великобритания)",
            "Бирмингем (Великобритания)",
            "Бристоль (Великобритания)",
            "Глазго (Великобритания)",
            "Дарем Долина Тиса (Великобритания)",
            "Кардифф (Великобритания)",
            "Лидс (Великобритания)",
            "Лондон (Великобритания)",
            "Манчестер (Великобритания)",
            "Норвич (Великобритания)",
            "Ньюкасл (Великобритания)",
            "Саутгемптон (Великобритания)",
            "Хамберсайд (Великобритания)",
            "Эдинбург (Великобритания)",
            "Будапешт (Венгрия)",
            "Берлин (Германия)",
            "Гамбург (Германия)",
            "Ганновер (Германия)",
            "Дрезден (Германия)",
            "Дюссельдорф (Германия)",
            "Кёльн (Германия)",
            "Мюнхен (Германия)",
            "Немецкие железные дороги (Германия)",
            "Франкфурт-на-Майне (Германия)",
            "Штутгарт (Германия)",
            "Александруполис (Греция)",
            "Афины (Греция)",
            "Ираклион (Греция)",
            "Корфу (Греция)",
            "Кос (Греция)",
            "Родос (Греция)",
            "Салоники (Греция)",
            "Санторини (Греция)",
            "Биллунд (Дания)",
            "Копенгаген (Дания)",
            "Ольборг (Дания)",
            "Дублин (Ирландия, Ирландская Республика)",
            "Рейкьявик (Исландия)",
            "Аликанте (Испания)",
            "Барселона (Испания)",
            "Бильбао (Испания)",
            "Валенсия (Испания)",
            "Виго (Испания)",
            "Ибица (Испания)",
            "Ла Корунья (Испания)",
            "Ланзароте (Испания)",
            "Лас-Пальмас-де-Гран-Канария (Испания)",
            "Мадрид (Испания)",
            "Малага (Испания)",
            "Менорка (Испания)",
            "Пальма Мальорка (Испания)",
            "Тенерифе (Испания)",
            "Фуэртевентура (Испания)",
            "Херес (Испания)",
            "Анкона (Италия)",
            "Бари (Италия)",
            "Болонья (Италия)",
            "Бриндизи (Италия)",
            "Венеция (Италия)",
            "Верона (Италия)",
            "Генуя (Италия)",
            "Кальяри (Италия)",
            "Катания (Италия)",
            "Ламеция-Терме (Италия)",
            "Милан (Италия)",
            "Неаполь (Италия)",
            "Палермо (Италия)",
            "Перуджа (Италия)",
            "Пескара (Италия)",
            "Пиза (Италия)",
            "Реджо-ди-Калабрия (Италия)",
            "Рим (Италия)",
            "Римини (Италия)",
            "Триест (Италия)",
            "Турин (Италия)",
            "Флоренция (Италия)",
            "Ларнака (Кипр)",
            "Пафос (Кипр)",
            "Эркан (Кипр)",
            "Рига (Латвия)",
            "Вильнюс (Литва)",
            "Каунас (Литва)",
            "Люксембург (Люксембург)",
            "Скопье (Македония)",
            "Мальта (Мальта)",
            "Амстердам (Нидерланды)",
            "Берген (Норвегия)",
            "Кристиансанд (Норвегия)",
            "Олесунн (Норвегия)",
            "Осло (Норвегия)",
            "Ставангер (Норвегия)",
            "Трондхайм (Норвегия)",
            "Варшава (Польша)",
            "Вроцлав (Польша)",
            "Гданьск (Польша)",
            "Краков (Польша)",
            "Познань (Польша)",
            "Лиссабон (Португалия)",
            "Бухарест (Румыния)",
            "Белград (Сербия)",
            "Приштина (Сербия)",
            "Братислава (Словакия)",
            "Жилина (Словакия)",
            "Кошице (Словакия)",
            "Любляна (Словения)",
            "Адана (Турция)",
            "Анталья (Турция)",
            "Бодрум (Турция)",
            "Даламан (Турция)",
            "Измир (Турция)",
            "Кайсери (Турция)",
            "Стамбул (Турция)",
            "Йоенсуу (Финляндия)",
            "Куопио (Финляндия)",
            "Оулу (Финляндия)",
            "Рованиеми (Финляндия)",
            "Тампере (Финляндия)",
            "Хельсинки (Финляндия)",
            "Биарриц (Франция)",
            "Бордо (Франция)",
            "Брест (Франция)",
            "Клермон-Ферран (Франция)",
            "Лилль (Франция)",
            "Лион (Франция)",
            "Марсель (Франция)",
            "Монпелье (Франция)",
            "Нант (Франция)",
            "Ницца (Франция)",
            "Париж (Франция)",
            "По (Франция)",
            "Реймс (Франция)",
            "Страсбург (Франция)",
            "Тулуза (Франция)",
            "Дубровник (Хорватия)",
            "Загреб (Хорватия)",
            "Задар (Хорватия)",
            "Пула (Хорватия)",
            "Сплит (Хорватия)",
            "Подгорица (Черногория)",
            "Тиват (Черногория)",
            "Карловы Вары (Чехия)",
            "Острава (Чехия)",
            "Прага (Чехия)",
            "Женева (Швейцария)",
            "Цюрих (Швейцария)",
            "Гетеборг (Швеция)",
            "Стокгольм (Швеция)",
            "Таллин (Эстония)"});
            this.CityFrom.Location = new System.Drawing.Point(130, 110);
            this.CityFrom.Name = "CityFrom";
            this.CityFrom.Size = new System.Drawing.Size(202, 21);
            this.CityFrom.TabIndex = 21;
            // 
            // CityTo
            // 
            this.CityTo.FormattingEnabled = true;
            this.CityTo.Location = new System.Drawing.Point(130, 143);
            this.CityTo.Name = "CityTo";
            this.CityTo.Size = new System.Drawing.Size(202, 21);
            this.CityTo.TabIndex = 20;
            // 
            // To
            // 
            this.To.AutoSize = true;
            this.To.Location = new System.Drawing.Point(54, 151);
            this.To.Name = "To";
            this.To.Size = new System.Drawing.Size(34, 13);
            this.To.TabIndex = 17;
            this.To.Text = "Куда:";
            // 
            // From
            // 
            this.From.AutoSize = true;
            this.From.Location = new System.Drawing.Point(54, 118);
            this.From.Name = "From";
            this.From.Size = new System.Drawing.Size(46, 13);
            this.From.TabIndex = 16;
            this.From.Text = "Откуда:";
            // 
            // OneDirection
            // 
            this.OneDirection.FlatAppearance.BorderSize = 9;
            this.OneDirection.Location = new System.Drawing.Point(121, 41);
            this.OneDirection.Name = "OneDirection";
            this.OneDirection.Size = new System.Drawing.Size(75, 45);
            this.OneDirection.TabIndex = 15;
            this.OneDirection.Text = "В одну сторону";
            this.OneDirection.UseVisualStyleBackColor = true;
            this.OneDirection.Click += new System.EventHandler(this.OneDirection_Click);
            // 
            // ThereAndBack
            // 
            this.ThereAndBack.FlatAppearance.BorderSize = 9;
            this.ThereAndBack.Location = new System.Drawing.Point(202, 41);
            this.ThereAndBack.Name = "ThereAndBack";
            this.ThereAndBack.Size = new System.Drawing.Size(75, 45);
            this.ThereAndBack.TabIndex = 14;
            this.ThereAndBack.Text = "Туда и обратно";
            this.ThereAndBack.UseVisualStyleBackColor = true;
            this.ThereAndBack.Click += new System.EventHandler(this.ThereAndBack_Click);
            // 
            // DateThereChoose
            // 
            this.DateThereChoose.Checked = false;
            this.DateThereChoose.Location = new System.Drawing.Point(39, 198);
            this.DateThereChoose.MaxDate = new System.DateTime(2100, 12, 31, 0, 0, 0, 0);
            this.DateThereChoose.MinDate = new System.DateTime(2018, 1, 1, 0, 0, 0, 0);
            this.DateThereChoose.Name = "DateThereChoose";
            this.DateThereChoose.Size = new System.Drawing.Size(148, 20);
            this.DateThereChoose.TabIndex = 0;
            // 
            // Head
            // 
            this.Head.AutoSize = true;
            this.Head.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Head.Location = new System.Drawing.Point(24, 9);
            this.Head.Name = "Head";
            this.Head.Size = new System.Drawing.Size(356, 29);
            this.Head.TabIndex = 13;
            this.Head.Text = "Бронирование авиабилетов";
            this.Head.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Client_User_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(958, 681);
            this.Controls.Add(this.TicketBookingPanel);
            this.Controls.Add(this.BuyTicketsPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Client_User_Form";
            this.Text = "PandAir - Добро пожаловать!";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Client_User_Form_FormClosed);
            this.TicketBookingPanel.ResumeLayout(false);
            this.TicketBookingPanel.PerformLayout();
            this.BabiesGroup.ResumeLayout(false);
            this.BabiesGroup.PerformLayout();
            this.ChildrenGroup.ResumeLayout(false);
            this.ChildrenGroup.PerformLayout();
            this.AdultsGroup.ResumeLayout(false);
            this.AdultsGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel BuyTicketsPanel;
        private System.Windows.Forms.ComboBox ClassOfServiceChoose;
        private System.Windows.Forms.Label ClassOfService;
        private System.Windows.Forms.RadioButton ThreeBabies;
        private System.Windows.Forms.RadioButton TwoBabies;
        private System.Windows.Forms.RadioButton OneBaby;
        private System.Windows.Forms.RadioButton NoBabies;
        private System.Windows.Forms.RadioButton FiveChildren;
        private System.Windows.Forms.RadioButton FourChildren;
        private System.Windows.Forms.RadioButton ThreeChildren;
        private System.Windows.Forms.RadioButton TwoChildren;
        private System.Windows.Forms.RadioButton OneChild;
        private System.Windows.Forms.RadioButton NoChildren;
        private System.Windows.Forms.RadioButton SixAdult;
        private System.Windows.Forms.RadioButton FiveAdult;
        private System.Windows.Forms.RadioButton FourAdult;
        private System.Windows.Forms.RadioButton ThreeAdult;
        private System.Windows.Forms.RadioButton TwoAdult;
        private System.Windows.Forms.Label DateBack;
        private System.Windows.Forms.Label DateThere;
        private System.Windows.Forms.DateTimePicker DateBackChoose;
        private System.Windows.Forms.ComboBox CityFrom;
        private System.Windows.Forms.ComboBox CityTo;
        private System.Windows.Forms.Label To;
        private System.Windows.Forms.Label From;
        private System.Windows.Forms.Button OneDirection;
        private System.Windows.Forms.Button ThereAndBack;
        private System.Windows.Forms.DateTimePicker DateThereChoose;
        private System.Windows.Forms.Label Head;
        private System.Windows.Forms.Button FindFlights;
        private System.Windows.Forms.Label InfoLabel;
        private System.Windows.Forms.Label Information2;
        private System.Windows.Forms.Label Information1;
        private System.Windows.Forms.GroupBox BabiesGroup;
        private System.Windows.Forms.GroupBox ChildrenGroup;
        private System.Windows.Forms.GroupBox AdultsGroup;
        private System.Windows.Forms.RadioButton OneAdult;
        private System.Windows.Forms.Button BackToMenu;
        public System.Windows.Forms.Panel TicketBookingPanel;
    }
}