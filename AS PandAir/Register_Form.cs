﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AS_PandAir
{
    public partial class Register_Form : Form
    {
        private LogIn_Form _LogForm;
        public Register_Form(LogIn_Form LogForm)
        {
            _LogForm = LogForm;
            InitializeComponent();
        }

        private void Register_Click(object sender, EventArgs e)
        {
            //проверка кода администратора и пароля, потом добавление сотрудника в БД
            MessageBox.Show("Сотрудник успешно добавлен!");
            this.Visible = false;
            _LogForm.Visible = true;
        }
    }
}
