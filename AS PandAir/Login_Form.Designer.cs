﻿namespace AS_PandAir
{
    partial class LogIn_Form
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LogIn_Form));
            this.LogPanel = new System.Windows.Forms.Panel();
            this.LogInPanel = new System.Windows.Forms.Panel();
            this.LogInButton = new System.Windows.Forms.Button();
            this.RegisterButton = new System.Windows.Forms.Button();
            this.PasswordText = new System.Windows.Forms.TextBox();
            this.IDText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.RegisterPanel = new System.Windows.Forms.Panel();
            this.RegPanel = new System.Windows.Forms.Panel();
            this.BackToLogInButton = new System.Windows.Forms.Button();
            this.RegButton = new System.Windows.Forms.Button();
            this.RegPasswordValid = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.RegPassword = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.RegID = new System.Windows.Forms.TextBox();
            this.LogPanel.SuspendLayout();
            this.LogInPanel.SuspendLayout();
            this.RegisterPanel.SuspendLayout();
            this.RegPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // LogPanel
            // 
            this.LogPanel.Controls.Add(this.LogInPanel);
            this.LogPanel.Controls.Add(this.RegisterPanel);
            this.LogPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LogPanel.Location = new System.Drawing.Point(0, 0);
            this.LogPanel.Name = "LogPanel";
            this.LogPanel.Size = new System.Drawing.Size(464, 283);
            this.LogPanel.TabIndex = 3;
            // 
            // LogInPanel
            // 
            this.LogInPanel.BackColor = System.Drawing.SystemColors.Control;
            this.LogInPanel.Controls.Add(this.LogInButton);
            this.LogInPanel.Controls.Add(this.RegisterButton);
            this.LogInPanel.Controls.Add(this.PasswordText);
            this.LogInPanel.Controls.Add(this.IDText);
            this.LogInPanel.Controls.Add(this.label2);
            this.LogInPanel.Controls.Add(this.label1);
            this.LogInPanel.Location = new System.Drawing.Point(112, 60);
            this.LogInPanel.Name = "LogInPanel";
            this.LogInPanel.Size = new System.Drawing.Size(240, 164);
            this.LogInPanel.TabIndex = 0;
            // 
            // LogInButton
            // 
            this.LogInButton.Location = new System.Drawing.Point(157, 141);
            this.LogInButton.Name = "LogInButton";
            this.LogInButton.Size = new System.Drawing.Size(80, 23);
            this.LogInButton.TabIndex = 1;
            this.LogInButton.Text = "Войти";
            this.LogInButton.UseVisualStyleBackColor = true;
            this.LogInButton.Click += new System.EventHandler(this.LogInButton_Click);
            // 
            // RegisterButton
            // 
            this.RegisterButton.Location = new System.Drawing.Point(6, 141);
            this.RegisterButton.Name = "RegisterButton";
            this.RegisterButton.Size = new System.Drawing.Size(80, 23);
            this.RegisterButton.TabIndex = 2;
            this.RegisterButton.Text = "Регистрация";
            this.RegisterButton.UseVisualStyleBackColor = true;
            this.RegisterButton.Click += new System.EventHandler(this.RegisterButton_Click);
            // 
            // PasswordText
            // 
            this.PasswordText.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PasswordText.Location = new System.Drawing.Point(6, 99);
            this.PasswordText.MaxLength = 255;
            this.PasswordText.Name = "PasswordText";
            this.PasswordText.Size = new System.Drawing.Size(231, 20);
            this.PasswordText.TabIndex = 3;
            this.PasswordText.UseSystemPasswordChar = true;
            this.PasswordText.WordWrap = false;
            // 
            // IDText
            // 
            this.IDText.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.IDText.Location = new System.Drawing.Point(6, 37);
            this.IDText.MaxLength = 255;
            this.IDText.Name = "IDText";
            this.IDText.Size = new System.Drawing.Size(231, 20);
            this.IDText.TabIndex = 2;
            this.IDText.WordWrap = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Введите пароль:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Введите ID:";
            // 
            // RegisterPanel
            // 
            this.RegisterPanel.Controls.Add(this.RegPanel);
            this.RegisterPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RegisterPanel.Location = new System.Drawing.Point(0, 0);
            this.RegisterPanel.Name = "RegisterPanel";
            this.RegisterPanel.Size = new System.Drawing.Size(464, 283);
            this.RegisterPanel.TabIndex = 1;
            this.RegisterPanel.Visible = false;
            // 
            // RegPanel
            // 
            this.RegPanel.Controls.Add(this.BackToLogInButton);
            this.RegPanel.Controls.Add(this.RegButton);
            this.RegPanel.Controls.Add(this.RegPasswordValid);
            this.RegPanel.Controls.Add(this.label5);
            this.RegPanel.Controls.Add(this.RegPassword);
            this.RegPanel.Controls.Add(this.label4);
            this.RegPanel.Controls.Add(this.label3);
            this.RegPanel.Controls.Add(this.RegID);
            this.RegPanel.Location = new System.Drawing.Point(91, 60);
            this.RegPanel.Name = "RegPanel";
            this.RegPanel.Size = new System.Drawing.Size(280, 164);
            this.RegPanel.TabIndex = 0;
            // 
            // BackToLogInButton
            // 
            this.BackToLogInButton.Location = new System.Drawing.Point(3, 138);
            this.BackToLogInButton.Name = "BackToLogInButton";
            this.BackToLogInButton.Size = new System.Drawing.Size(75, 23);
            this.BackToLogInButton.TabIndex = 7;
            this.BackToLogInButton.Text = "Назад";
            this.BackToLogInButton.UseVisualStyleBackColor = true;
            this.BackToLogInButton.Click += new System.EventHandler(this.BackToLogInButton_Click);
            // 
            // RegButton
            // 
            this.RegButton.Location = new System.Drawing.Point(147, 138);
            this.RegButton.Name = "RegButton";
            this.RegButton.Size = new System.Drawing.Size(130, 23);
            this.RegButton.TabIndex = 6;
            this.RegButton.Text = "Зарегистрироваться";
            this.RegButton.UseVisualStyleBackColor = true;
            this.RegButton.Click += new System.EventHandler(this.RegButton_Click);
            // 
            // RegPasswordValid
            // 
            this.RegPasswordValid.Location = new System.Drawing.Point(0, 102);
            this.RegPasswordValid.Name = "RegPasswordValid";
            this.RegPasswordValid.Size = new System.Drawing.Size(279, 20);
            this.RegPasswordValid.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 86);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(136, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Введите пароль еще раз:";
            // 
            // RegPassword
            // 
            this.RegPassword.Location = new System.Drawing.Point(0, 63);
            this.RegPassword.Name = "RegPassword";
            this.RegPassword.Size = new System.Drawing.Size(279, 20);
            this.RegPassword.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Введите пароль:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Введите ID:";
            // 
            // RegID
            // 
            this.RegID.Location = new System.Drawing.Point(0, 24);
            this.RegID.Name = "RegID";
            this.RegID.Size = new System.Drawing.Size(279, 20);
            this.RegID.TabIndex = 0;
            // 
            // LogIn_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 283);
            this.Controls.Add(this.LogPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LogIn_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Вход";
            this.LogPanel.ResumeLayout(false);
            this.LogInPanel.ResumeLayout(false);
            this.LogInPanel.PerformLayout();
            this.RegisterPanel.ResumeLayout(false);
            this.RegPanel.ResumeLayout(false);
            this.RegPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel LogPanel;
        private System.Windows.Forms.Panel LogInPanel;
        private System.Windows.Forms.Button LogInButton;
        private System.Windows.Forms.Button RegisterButton;
        private System.Windows.Forms.TextBox PasswordText;
        private System.Windows.Forms.TextBox IDText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel RegisterPanel;
        private System.Windows.Forms.Panel RegPanel;
        private System.Windows.Forms.Button BackToLogInButton;
        private System.Windows.Forms.Button RegButton;
        private System.Windows.Forms.TextBox RegPasswordValid;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox RegPassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox RegID;
    }
}

