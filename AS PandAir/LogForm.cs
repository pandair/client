﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AS_PandAir
{
    public partial class LogIn_Form : Form
    {
        private ForgotPassword forgPassw;
        private Register_Form regForm;
        private Client_User_Form clientForm;
        private CashierForm cashierForm;
        public LogIn_Form()
        {
            InitializeComponent();
            forgPassw = new ForgotPassword(this) { Visible = false };
            regForm = new Register_Form(this) { Visible = false };
            clientForm = new Client_User_Form(this) { Visible = false };
            cashierForm = new CashierForm(this) { Visible = false };
        }

        private void Authorization_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            if (Login.Text == "Admin" && Password.Text == "12345")
            {
                //админская панель
            }
            else
            {
                cashierForm.Visible = true;
            }
        }
        
        //но по-хорошему ему надо высылать на почту логин-пароль
        private void ForgotPassword_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Visible = false;
            forgPassw.Visible = true;

        }

        private void MakeOrder_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            clientForm.Visible = true;
        }

        private void SignUp_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Visible = false;
            regForm.Visible = true;
        }
    }
}
