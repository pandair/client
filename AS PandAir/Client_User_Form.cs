﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AS_PandAir
{
    public partial class Client_User_Form : Form
    {
        bool DoubleTicket;
        private LogIn_Form _LogForm;

        public Client_User_Form(LogIn_Form LogForm)
        {
            _LogForm = LogForm;
            InitializeComponent();
            ClassOfServiceChoose.SelectedIndex = 0;
            TwoBabies.Enabled = false;
            ThreeBabies.Enabled = false;
        }

        private void BackToMenu_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            _LogForm.Visible = true;
        }

        private void Client_User_Form_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void ThereAndBack_Click(object sender, EventArgs e)
        {
            DateBackChoose.Enabled = true;
            DoubleTicket = true;
        }

        private void OneDirection_Click(object sender, EventArgs e)
        {
            DateBackChoose.Enabled = false;
            DoubleTicket = false;
        }

        private void OneAdult_CheckedChanged(object sender, EventArgs e)
        {
            OneChild.Enabled = true;
            TwoChildren.Enabled = true;
            ThreeChildren.Enabled = true;
            FourChildren.Enabled = true;
            FiveChildren.Enabled = true;
            OneBaby.Enabled = true;
            TwoBabies.Enabled = false;
            ThreeBabies.Enabled = false;
            if (FiveChildren.Checked)
                OneBaby.Enabled = false;
            else
                OneBaby.Enabled = true;
            if (OneBaby.Checked)
                FiveChildren.Enabled = false;
            else
                FiveChildren.Enabled = true;
            if (OneChild.Checked)
                SixAdult.Enabled = false;
            else
                SixAdult.Enabled = true;
            if (TwoChildren.Checked)
            {
                FiveAdult.Enabled = false;
                SixAdult.Enabled = false;
            }
            else
            {
                FiveAdult.Enabled = true;
                SixAdult.Enabled = true;
            }
            if (ThreeChildren.Checked)
            {
                FourAdult.Enabled = false;
                FiveAdult.Enabled = false;
                SixAdult.Enabled = false;
            }
            else
            {
                FourAdult.Enabled = true;
                FiveAdult.Enabled = true;
                SixAdult.Enabled = true;
            }
            if (FourChildren.Checked)
            {
                ThreeAdult.Enabled = false;
                FourAdult.Enabled = false;
                FiveAdult.Enabled = false;
                SixAdult.Enabled = false;
            }
            else
            {
                ThreeAdult.Enabled = true;
                FourAdult.Enabled = true;
                FiveAdult.Enabled = true;
                SixAdult.Enabled = true;
            }
            if (FiveChildren.Checked)
            {
                TwoAdult.Enabled = false;
                ThreeAdult.Enabled = false;
                FourAdult.Enabled = false;
                FiveAdult.Enabled = false;
                SixAdult.Enabled = false;
            }
            else
            {
                TwoAdult.Enabled = true;
                ThreeAdult.Enabled = true;
                FourAdult.Enabled = true;
                FiveAdult.Enabled = true;
                SixAdult.Enabled = true;
            }
            if (OneBaby.Checked)
            {
                SixAdult.Enabled = false;
                FiveChildren.Enabled = false;
            }
            else
            {
                SixAdult.Enabled = true;
                FiveChildren.Enabled = true;
            }
        }

        private void TwoAdults_CheckedChanged(object sender, EventArgs e)
        {
            OneBaby.Enabled = true;
            TwoBabies.Enabled = true;
            ThreeBabies.Enabled = false;
            OneChild.Enabled = true;
            TwoChildren.Enabled = true;
            ThreeChildren.Enabled = true;
            FourChildren.Enabled = true;
            FiveChildren.Enabled = false;
            if (OneChild.Checked)
            {
                SixAdult.Enabled = false;
            }
            else
            {
                SixAdult.Enabled = true;
            }
            if (TwoChildren.Checked)
            {
                FiveAdult.Enabled = false;
                SixAdult.Enabled = false;
            }
            else
            {
                FiveAdult.Enabled = true;
                SixAdult.Enabled = true;
            }
            if (ThreeChildren.Checked)
            {
                TwoBabies.Enabled = false;
                FourAdult.Enabled = false;
                FiveAdult.Enabled = false;
                SixAdult.Enabled = false;
            }
            else
            {
                TwoBabies.Enabled = true;
                FourAdult.Enabled = true;
                FiveAdult.Enabled = true;
                SixAdult.Enabled = true;
            }
            if (FourChildren.Checked)
            {
                OneBaby.Enabled = false;
                TwoBabies.Enabled = false;
                ThreeAdult.Enabled = false;
                FourAdult.Enabled = false;
                FiveAdult.Enabled = false;
                SixAdult.Enabled = false;
            }
            else
            {
                OneBaby.Enabled = true;
                TwoBabies.Enabled = true;
                ThreeAdult.Enabled = true;
                FourAdult.Enabled = true;
                FiveAdult.Enabled = true;
                SixAdult.Enabled = true;
            }
            if (OneBaby.Checked)
            {
                FourChildren.Enabled = false;
                SixAdult.Enabled = false;
            }
            else
            {
                FourChildren.Enabled = true;
                SixAdult.Enabled = true;
            }
            if (TwoBabies.Checked)
            {
                ThreeChildren.Enabled = false;
                FourChildren.Enabled = false;
                FiveAdult.Enabled = false;
                SixAdult.Enabled = false;
            }
            else
            {
                ThreeChildren.Enabled = true;
                FourChildren.Enabled = true;
                FiveAdult.Enabled = true;
                SixAdult.Enabled = true;
            }
        }

        private void ThreeAdults_CheckedChanged(object sender, EventArgs e)
        {
            OneBaby.Enabled = true;
            TwoBabies.Enabled = true;
            ThreeBabies.Enabled = true;
            OneChild.Enabled = true;
            TwoChildren.Enabled = true;
            ThreeChildren.Enabled = true;
            FourChildren.Enabled = false;
            FiveChildren.Enabled = false;

            if (OneChild.Checked)
            {
                ThreeBabies.Enabled = false;
                SixAdult.Enabled = false;
            }
            else
            {
                SixAdult.Enabled = true;
                ThreeBabies.Enabled = true;
            }
            if (TwoChildren.Checked)
            {
                TwoBabies.Enabled = false;
                ThreeBabies.Enabled = false;
                FiveAdult.Enabled = false;
                SixAdult.Enabled = false;
            }
            else
            {
                ThreeBabies.Enabled = true;
                TwoBabies.Enabled = true;
                FiveAdult.Enabled = true;
                SixAdult.Enabled = true;
            }
            if (ThreeChildren.Checked)
            {
                OneBaby.Enabled = false;
                TwoBabies.Enabled = false;
                ThreeBabies.Enabled = false;
                FourAdult.Enabled = false;
                FiveAdult.Enabled = false;
                SixAdult.Enabled = false;
            }
            else
            {
                OneBaby.Enabled = true;
                TwoBabies.Enabled = true;
                ThreeBabies.Enabled = true;
                FourAdult.Enabled = true;
                FiveAdult.Enabled = true;
                SixAdult.Enabled = true;
            }
            if (OneBaby.Checked)
            {
                ThreeChildren.Enabled = false;
                SixAdult.Enabled = false;
            }
            else
            {
                ThreeChildren.Enabled = true;
                SixAdult.Enabled = true;
            }
            if (TwoBabies.Checked)
            {
                OneAdult.Enabled = false;
                TwoChildren.Enabled = false;
                ThreeChildren.Enabled = false;
                FiveAdult.Enabled = false;
                SixAdult.Enabled = false;
            }
            else
            {
                OneAdult.Enabled = true;
                TwoChildren.Enabled = true;
                ThreeChildren.Enabled = true;
                FiveAdult.Enabled = true;
                SixAdult.Enabled = true;
            }
            if (ThreeBabies.Checked)
            {
                OneAdult.Enabled = false;
                TwoAdult.Enabled = false;
                OneChild.Enabled = false;
                TwoChildren.Enabled = false;
                ThreeChildren.Enabled = false;
                FiveAdult.Enabled = false;
                SixAdult.Enabled = false;

            }
            else
            {
                OneAdult.Enabled = true;
                TwoAdult.Enabled = true;
                OneChild.Enabled = true;
                TwoChildren.Enabled = true;
                ThreeChildren.Enabled = true;
                FiveAdult.Enabled = true;
                SixAdult.Enabled = true;
            }

        }

            private void ProveDates(object sender, EventArgs e)
        {
            if ((DoubleTicket) && (DateBackChoose.Value.Date < DateThereChoose.Value.Date))
            {
                MessageBox.Show("Дата возвращения ранее даты отбытия!");
            }

        }

        private void FourAdult_CheckedChanged(object sender, EventArgs e)
        {
            ThreeChildren.Enabled = false;
            FourChildren.Enabled = false;
            FiveChildren.Enabled = false;
            ThreeBabies.Enabled = false;
            if (OneChild.Checked)
            {
                SixAdult.Enabled = false;
                TwoBabies.Enabled = false;
            }
            else
            {
                SixAdult.Enabled = true;
                TwoBabies.Enabled = true;
            }
            if (TwoChildren.Checked)
            {
                FiveAdult.Enabled = false;
                SixAdult.Enabled = false;
                OneBaby.Enabled = false;
                TwoBabies.Enabled = false;
            }
            else
            {
                FiveAdult.Enabled = true;
                SixAdult.Enabled = true;
                OneBaby.Enabled = true;
                TwoBabies.Enabled = true;
            }
            if (OneBaby.Checked)
            {
                SixAdult.Enabled = false;
                TwoChildren.Enabled = false;
            }
            else
            {
                SixAdult.Enabled = true;
                TwoChildren.Enabled = true;
            }
            if (TwoBabies.Checked)
            {
                OneChild.Enabled = false;
                TwoChildren.Enabled = false;
                SixAdult.Enabled = false;
                FiveAdult.Enabled = false;
            }
            else
            {
                OneChild.Enabled = true;
                TwoChildren.Enabled = true;
                SixAdult.Enabled = true;
                FiveAdult.Enabled = true;
            }
        }

        private void FiveAdult_CheckedChanged(object sender, EventArgs e)
        {
            TwoChildren.Enabled = false;
            ThreeChildren.Enabled = false;
            FourChildren.Enabled = false;
            FiveChildren.Enabled = false;
            TwoBabies.Enabled = false;
            ThreeBabies.Enabled = false;
            if (OneChild.Checked)
            {
                OneBaby.Enabled = false;
                SixAdult.Enabled = false;
            }
            else
            {
                OneBaby.Enabled = true;
                SixAdult.Enabled = true;
            }
            if (OneBaby.Checked)
            {
                OneChild.Enabled = false;
                SixAdult.Enabled = false;
            }
            else
            {
                OneChild.Enabled = true;
                SixAdult.Enabled = true;
            }
        }

        private void SixAdult_CheckedChanged(object sender, EventArgs e)
        {
            OneChild.Enabled = false;
            TwoChildren.Enabled = false;
            ThreeChildren.Enabled = false;
            FourChildren.Enabled = false;
            FiveChildren.Enabled = false;
            OneBaby.Enabled = false;
            TwoBabies.Enabled = false;
            ThreeBabies.Enabled = false;
        }

        private void OneChild_CheckedChanged(object sender, EventArgs e)
        {
            SixAdult.Enabled = false;
            if (OneBaby.Checked)
            {
                FiveChildren.Enabled = false;
                FiveAdult.Enabled = false;
            }
            else
            {
                FiveChildren.Enabled = true;
                FiveAdult.Enabled = true;
            }
            if (TwoAdult.Checked)
            {
                TwoBabies.Enabled = true;
                FiveChildren.Enabled = false;
            }
            else
            {
                TwoBabies.Enabled = false;
                FiveChildren.Enabled = true;
            }
            if (ThreeAdult.Checked)
            {
                FourChildren.Enabled = false;
            }
            else
            {
                FourChildren.Enabled = true;
            }
            if (FourAdult.Checked)
            {
                ThreeChildren.Enabled = false;
                TwoBabies.Enabled = false;
            }
            else
            {
                ThreeChildren.Enabled = true;
                TwoBabies.Enabled = true;
            }
            if (FiveAdult.Checked)
            {
                OneBaby.Enabled = false;
                TwoChildren.Enabled = false;
            }
            else
            {
                OneBaby.Enabled = true;
                TwoChildren.Enabled = true;
            }
        }

        private void TwoChildren_CheckedChanged(object sender, EventArgs e)
        {
            FiveAdult.Enabled = false;
            SixAdult.Enabled = false;
            if (OneBaby.Checked)
            {
                FourAdult.Enabled = false;
                FiveChildren.Enabled = false;
            }
            else
            {
                FourAdult.Enabled = true;
                FiveChildren.Enabled = true;
            }
            if (TwoAdult.Checked)
            {
                TwoBabies.Enabled = true;
                FiveChildren.Enabled = false;
            }
            else
            {
                TwoBabies.Enabled = false;
                FiveChildren.Enabled = true;
            }
            if (ThreeAdult.Checked)
            {
                FourChildren.Enabled = false;
                TwoBabies.Enabled = false;
            }
            else
            {
                FourChildren.Enabled = true;
                TwoBabies.Enabled = true;
            }
            if (FourAdult.Checked)
            {
                ThreeChildren.Enabled = false;
                OneBaby.Enabled = false;
            }
            else
            {
                ThreeChildren.Enabled = true;
                OneBaby.Enabled = true;
            }
        }

        private void ThreeChildren_CheckedChanged(object sender, EventArgs e)
        {
            FourAdult.Enabled = false;
            FiveAdult.Enabled = false;
            SixAdult.Enabled = false;
            if (OneBaby.Checked)
            {
                FiveChildren.Enabled = false;
                ThreeAdult.Enabled = false;
            }
            else
            {
                FiveChildren.Enabled = true;
                ThreeAdult.Enabled = true;
            }
            if (TwoAdult.Checked)
            {
                FiveChildren.Enabled = false;
            }
            else
            {
                FiveChildren.Enabled = true;
            }
            if (ThreeAdult.Checked)
            {
                FourChildren.Enabled = false;
                OneBaby.Enabled = false;
            }
            else
            {
                FourChildren.Enabled = true;
                OneBaby.Enabled = true;
            }
        }

        private void FourChildren_CheckedChanged(object sender, EventArgs e)
        {
            ThreeAdult.Enabled = false;
            FourAdult.Enabled = false;
            FiveAdult.Enabled = false;
            SixAdult.Enabled = false;
            if (OneBaby.Checked)
            {
                TwoAdult.Enabled = false;
                FiveChildren.Enabled = false;
            }
            else
            {
                TwoAdult.Enabled = true;
                FiveChildren.Enabled = true;
            }
            if (TwoAdult.Checked)
            {
                FiveChildren.Enabled = false;
                OneBaby.Enabled = false;
            }
            else
            {
                FiveChildren.Enabled = true;
                OneBaby.Enabled = true;
            }
        }

        private void FiveChildren_CheckedChanged(object sender, EventArgs e)
        {
            TwoAdult.Enabled = false;
            ThreeAdult.Enabled = false;
            FourAdult.Enabled = false;
            FiveAdult.Enabled = false;
            SixAdult.Enabled = false;
            OneBaby.Enabled = false;
        }

        private void OneBaby_CheckedChanged(object sender, EventArgs e)
        {
            SixAdult.Enabled = false;
            FiveChildren.Enabled = false;
            if (OneChild.Checked)
            {
                FiveAdult.Enabled = false;
            }
            else
            {
                FiveAdult.Enabled = true;
            }
            if (TwoChildren.Checked)
            {
                FourAdult.Enabled = false;
            }
            else
            {
                FourAdult.Enabled = true;
            }
            if (ThreeChildren.Checked)
            {
                ThreeAdult.Enabled = false;
            }
            else
            {
                ThreeAdult.Enabled = true;
            }
            if (FourChildren.Checked)
            {
                TwoAdult.Enabled = false;
            }
            else
            {
                TwoAdult.Enabled = true;
            }
            if (TwoAdult.Checked)
            {
                TwoBabies.Enabled = true;
                FourChildren.Enabled = false;
            }
            else
            {
                TwoBabies.Enabled = false;
                FourChildren.Enabled = true;
            }
            if (ThreeAdult.Checked)
            {
                ThreeChildren.Enabled = false;
                ThreeBabies.Enabled = true;
            }
            else
            {
                ThreeChildren.Enabled = true;
                ThreeBabies.Enabled = false;
            }
            if (FourAdult.Checked)
            {
                TwoChildren.Enabled = false;
                ThreeBabies.Enabled = false;
            }
            else
            {
                TwoChildren.Enabled = true;
                ThreeBabies.Enabled = true;
            }
            if (FiveAdult.Checked)
            {
                OneChild.Enabled = false;
                TwoBabies.Enabled = false;
            }
            else
            {
                OneChild.Enabled = true;
                TwoBabies.Enabled = true;
            }
        }

        private void TwoBabies_CheckedChanged(object sender, EventArgs e)
        {
            OneAdult.Enabled = false;
            FiveAdult.Enabled = false;
            SixAdult.Enabled = false;
            ThreeChildren.Enabled = false;
            if (ThreeAdult.Checked)
            {
                TwoChildren.Enabled = false;
                ThreeBabies.Enabled = true;
            }
            else
            {
                TwoChildren.Enabled = true;
                ThreeBabies.Enabled = false;
            }
            if (FourAdult.Checked)
            {
                ThreeBabies.Enabled = false;
                OneChild.Enabled = false;
            }
            else
            {
                ThreeBabies.Enabled = true;
                OneChild.Enabled = true;
            }
            if (OneChild.Checked)
            {
                FourAdult.Enabled = false;
            }
            else
            {
                FourAdult.Enabled = true;
            }
            if (TwoChildren.Checked)
            {
                ThreeAdult.Enabled = false;
            }
            else
            {
                ThreeAdult.Enabled = true;
            }
        }

        private void ThreeBabies_CheckedChanged(object sender, EventArgs e)
        {
            OneAdult.Enabled = false;
            TwoAdult.Enabled = false;
            FourAdult.Enabled = false;
            FiveAdult.Enabled = false;
            SixAdult.Enabled = false;
            OneChild.Enabled = false;
        }
    }


}
