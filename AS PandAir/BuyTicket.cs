﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AS_PandAir
{
    public partial class BuyTicket : Form
    {
        private CashierForm _CashForm;
        public BuyTicket(CashierForm CashForm)
        {
            _CashForm = CashForm;
            InitializeComponent();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            _CashForm.Visible = true;
        }
    }
}
