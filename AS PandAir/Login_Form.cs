﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AS_PandAir
{
    public partial class LogIn_Form : Form
    {
        public LogIn_Form()
        {
            InitializeComponent();
        }

        private void RegisterButton_Click(object sender, EventArgs e)
        {
            RegisterPanel.Visible = true;
            LogInPanel.Visible = false;
        }

        private void BackToLogInButton_Click(object sender, EventArgs e)
        {
            LogInPanel.Visible = true;
            RegisterPanel.Visible = false;
        }

        private void LogInButton_Click(object sender, EventArgs e)
        {
            Client_User_Form ClUF = new Client_User_Form();
            /*
             * Код для сверки с сервером, получения токена и входа в программу.
             * 
             */
            Hide();
            // if TOKEN = USER then
            ClUF.Show();
            // else if TOKEN = ADMINISTRATOR then
            // Client_Admin_Form ClAF = new Client_Admin_Form();
            // ClAF.Show();
        }

        private void RegButton_Click(object sender, EventArgs e)
        {
            /*
             Регистрация нового пользователя
             
             */
        }
    }
}
