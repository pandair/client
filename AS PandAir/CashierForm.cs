﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AS_PandAir
{
    public partial class CashierForm : Form
    {
        private LogIn_Form _LogForm;
        private BuyTicket buyTicket;
        public CashierForm(LogIn_Form LogForm)
        {
            _LogForm = LogForm;
            InitializeComponent();
            buyTicket = new BuyTicket(this) { Visible = false };
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            _LogForm.Visible = true;
        }

        private void BuyTickets_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            buyTicket.Visible = true;
        }
    }
}
