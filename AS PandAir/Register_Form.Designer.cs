﻿namespace AS_PandAir
{
    partial class Register_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Register_Form));
            this.CashierSurname = new System.Windows.Forms.TextBox();
            this.CashierName = new System.Windows.Forms.TextBox();
            this.CashierMidName = new System.Windows.Forms.TextBox();
            this.CSurname = new System.Windows.Forms.Label();
            this.CName = new System.Windows.Forms.Label();
            this.CMidName = new System.Windows.Forms.Label();
            this.Register = new System.Windows.Forms.Button();
            this.CashierCode = new System.Windows.Forms.TextBox();
            this.CCode = new System.Windows.Forms.Label();
            this.AdministratorPassword = new System.Windows.Forms.TextBox();
            this.AdmPassw = new System.Windows.Forms.Label();
            this.CNumber = new System.Windows.Forms.Label();
            this.CashNumber = new System.Windows.Forms.TextBox();
            this.AdministratorCode = new System.Windows.Forms.TextBox();
            this.AdmCode = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CashierSurname
            // 
            this.CashierSurname.Location = new System.Drawing.Point(424, 21);
            this.CashierSurname.Name = "CashierSurname";
            this.CashierSurname.Size = new System.Drawing.Size(151, 20);
            this.CashierSurname.TabIndex = 0;
            // 
            // CashierName
            // 
            this.CashierName.Location = new System.Drawing.Point(424, 52);
            this.CashierName.Name = "CashierName";
            this.CashierName.Size = new System.Drawing.Size(151, 20);
            this.CashierName.TabIndex = 1;
            // 
            // CashierMidName
            // 
            this.CashierMidName.Location = new System.Drawing.Point(424, 82);
            this.CashierMidName.Name = "CashierMidName";
            this.CashierMidName.Size = new System.Drawing.Size(151, 20);
            this.CashierMidName.TabIndex = 2;
            // 
            // CSurname
            // 
            this.CSurname.AutoSize = true;
            this.CSurname.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.CSurname.Location = new System.Drawing.Point(271, 28);
            this.CSurname.Name = "CSurname";
            this.CSurname.Size = new System.Drawing.Size(59, 13);
            this.CSurname.TabIndex = 3;
            this.CSurname.Text = "Фамилия:";
            // 
            // CName
            // 
            this.CName.AutoSize = true;
            this.CName.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.CName.Location = new System.Drawing.Point(271, 59);
            this.CName.Name = "CName";
            this.CName.Size = new System.Drawing.Size(32, 13);
            this.CName.TabIndex = 4;
            this.CName.Text = "Имя:";
            // 
            // CMidName
            // 
            this.CMidName.AutoSize = true;
            this.CMidName.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.CMidName.Location = new System.Drawing.Point(271, 89);
            this.CMidName.Name = "CMidName";
            this.CMidName.Size = new System.Drawing.Size(57, 13);
            this.CMidName.TabIndex = 5;
            this.CMidName.Text = "Отчество:";
            // 
            // Register
            // 
            this.Register.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Register.Location = new System.Drawing.Point(340, 245);
            this.Register.Name = "Register";
            this.Register.Size = new System.Drawing.Size(156, 40);
            this.Register.TabIndex = 6;
            this.Register.Text = "Зарегистрировать";
            this.Register.UseVisualStyleBackColor = true;
            this.Register.Click += new System.EventHandler(this.Register_Click);
            // 
            // CashierCode
            // 
            this.CashierCode.Location = new System.Drawing.Point(424, 144);
            this.CashierCode.Name = "CashierCode";
            this.CashierCode.Size = new System.Drawing.Size(151, 20);
            this.CashierCode.TabIndex = 7;
            // 
            // CCode
            // 
            this.CCode.AutoSize = true;
            this.CCode.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.CCode.Location = new System.Drawing.Point(271, 151);
            this.CCode.Name = "CCode";
            this.CCode.Size = new System.Drawing.Size(90, 13);
            this.CCode.TabIndex = 8;
            this.CCode.Text = "Код сотрудника:";
            // 
            // AdministratorPassword
            // 
            this.AdministratorPassword.Location = new System.Drawing.Point(424, 206);
            this.AdministratorPassword.Name = "AdministratorPassword";
            this.AdministratorPassword.Size = new System.Drawing.Size(151, 20);
            this.AdministratorPassword.TabIndex = 9;
            // 
            // AdmPassw
            // 
            this.AdmPassw.AutoSize = true;
            this.AdmPassw.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.AdmPassw.Location = new System.Drawing.Point(271, 213);
            this.AdmPassw.Name = "AdmPassw";
            this.AdmPassw.Size = new System.Drawing.Size(135, 13);
            this.AdmPassw.TabIndex = 10;
            this.AdmPassw.Text = "Пароль администратора:";
            // 
            // CNumber
            // 
            this.CNumber.AutoSize = true;
            this.CNumber.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.CNumber.Location = new System.Drawing.Point(271, 119);
            this.CNumber.Name = "CNumber";
            this.CNumber.Size = new System.Drawing.Size(79, 13);
            this.CNumber.TabIndex = 11;
            this.CNumber.Text = "Номер кассы:";
            // 
            // CashNumber
            // 
            this.CashNumber.Location = new System.Drawing.Point(424, 108);
            this.CashNumber.Name = "CashNumber";
            this.CashNumber.Size = new System.Drawing.Size(151, 20);
            this.CashNumber.TabIndex = 12;
            // 
            // AdministratorCode
            // 
            this.AdministratorCode.Location = new System.Drawing.Point(424, 176);
            this.AdministratorCode.Name = "AdministratorCode";
            this.AdministratorCode.Size = new System.Drawing.Size(151, 20);
            this.AdministratorCode.TabIndex = 13;
            // 
            // AdmCode
            // 
            this.AdmCode.AutoSize = true;
            this.AdmCode.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.AdmCode.Location = new System.Drawing.Point(271, 183);
            this.AdmCode.Name = "AdmCode";
            this.AdmCode.Size = new System.Drawing.Size(116, 13);
            this.AdmCode.TabIndex = 14;
            this.AdmCode.Text = "Код администратора:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.label1.Location = new System.Drawing.Point(12, 290);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "PandAir 2018";
            // 
            // Register_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(599, 312);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AdmCode);
            this.Controls.Add(this.AdministratorCode);
            this.Controls.Add(this.CashNumber);
            this.Controls.Add(this.CNumber);
            this.Controls.Add(this.AdmPassw);
            this.Controls.Add(this.AdministratorPassword);
            this.Controls.Add(this.CCode);
            this.Controls.Add(this.CashierCode);
            this.Controls.Add(this.Register);
            this.Controls.Add(this.CMidName);
            this.Controls.Add(this.CName);
            this.Controls.Add(this.CSurname);
            this.Controls.Add(this.CashierMidName);
            this.Controls.Add(this.CashierName);
            this.Controls.Add(this.CashierSurname);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Register_Form";
            this.Text = "Регистрация сотрудника";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox CashierSurname;
        private System.Windows.Forms.TextBox CashierName;
        private System.Windows.Forms.TextBox CashierMidName;
        private System.Windows.Forms.Label CSurname;
        private System.Windows.Forms.Label CName;
        private System.Windows.Forms.Label CMidName;
        private System.Windows.Forms.Button Register;
        private System.Windows.Forms.TextBox CashierCode;
        private System.Windows.Forms.Label CCode;
        private System.Windows.Forms.TextBox AdministratorPassword;
        private System.Windows.Forms.Label AdmPassw;
        private System.Windows.Forms.Label CNumber;
        private System.Windows.Forms.TextBox CashNumber;
        private System.Windows.Forms.TextBox AdministratorCode;
        private System.Windows.Forms.Label AdmCode;
        private System.Windows.Forms.Label label1;
    }
}