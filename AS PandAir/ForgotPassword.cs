﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AS_PandAir
{
    public partial class ForgotPassword : Form
    {
        private LogIn_Form _LogForm;
        public ForgotPassword(LogIn_Form LogForm)
        {
            _LogForm = LogForm;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //отправка данных из текстбокса администратору
            MessageBox.Show("Заявка успешно отправлена!");
            this.Visible = false;
            _LogForm.Visible = true;
        }
    }
}
