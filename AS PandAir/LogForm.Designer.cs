﻿namespace AS_PandAir
{
    partial class LogIn_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LogIn_Form));
            this.Login = new System.Windows.Forms.TextBox();
            this.Password = new System.Windows.Forms.TextBox();
            this.LogLabel = new System.Windows.Forms.Label();
            this.PassWLabel = new System.Windows.Forms.Label();
            this.Authorization = new System.Windows.Forms.Button();
            this.ForgotPassword = new System.Windows.Forms.LinkLabel();
            this.SignUp = new System.Windows.Forms.LinkLabel();
            this.MakeOrder = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Login
            // 
            this.Login.Location = new System.Drawing.Point(354, 12);
            this.Login.Name = "Login";
            this.Login.Size = new System.Drawing.Size(166, 20);
            this.Login.TabIndex = 0;
            // 
            // Password
            // 
            this.Password.Location = new System.Drawing.Point(354, 40);
            this.Password.Name = "Password";
            this.Password.Size = new System.Drawing.Size(166, 20);
            this.Password.TabIndex = 1;
            // 
            // LogLabel
            // 
            this.LogLabel.AutoSize = true;
            this.LogLabel.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.LogLabel.Location = new System.Drawing.Point(273, 19);
            this.LogLabel.Name = "LogLabel";
            this.LogLabel.Size = new System.Drawing.Size(41, 13);
            this.LogLabel.TabIndex = 2;
            this.LogLabel.Text = "Логин:";
            // 
            // PassWLabel
            // 
            this.PassWLabel.AutoSize = true;
            this.PassWLabel.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.PassWLabel.Location = new System.Drawing.Point(273, 47);
            this.PassWLabel.Name = "PassWLabel";
            this.PassWLabel.Size = new System.Drawing.Size(48, 13);
            this.PassWLabel.TabIndex = 3;
            this.PassWLabel.Text = "Пароль:";
            // 
            // Authorization
            // 
            this.Authorization.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Authorization.Location = new System.Drawing.Point(318, 81);
            this.Authorization.Name = "Authorization";
            this.Authorization.Size = new System.Drawing.Size(158, 33);
            this.Authorization.TabIndex = 4;
            this.Authorization.Text = "Авторизоваться";
            this.Authorization.UseVisualStyleBackColor = true;
            this.Authorization.Click += new System.EventHandler(this.Authorization_Click);
            // 
            // ForgotPassword
            // 
            this.ForgotPassword.AutoSize = true;
            this.ForgotPassword.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.ForgotPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ForgotPassword.Location = new System.Drawing.Point(312, 130);
            this.ForgotPassword.Name = "ForgotPassword";
            this.ForgotPassword.Size = new System.Drawing.Size(187, 17);
            this.ForgotPassword.TabIndex = 5;
            this.ForgotPassword.TabStop = true;
            this.ForgotPassword.Text = "Забыли пароль или логин?";
            this.ForgotPassword.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ForgotPassword_LinkClicked);
            // 
            // SignUp
            // 
            this.SignUp.AutoSize = true;
            this.SignUp.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.SignUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SignUp.Location = new System.Drawing.Point(356, 156);
            this.SignUp.Name = "SignUp";
            this.SignUp.Size = new System.Drawing.Size(92, 17);
            this.SignUp.TabIndex = 6;
            this.SignUp.TabStop = true;
            this.SignUp.Text = "Регистрация";
            this.SignUp.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.SignUp_LinkClicked);
            // 
            // MakeOrder
            // 
            this.MakeOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MakeOrder.Location = new System.Drawing.Point(276, 192);
            this.MakeOrder.Name = "MakeOrder";
            this.MakeOrder.Size = new System.Drawing.Size(244, 36);
            this.MakeOrder.TabIndex = 7;
            this.MakeOrder.Text = "Бронирование билетов";
            this.MakeOrder.UseVisualStyleBackColor = true;
            this.MakeOrder.Click += new System.EventHandler(this.MakeOrder_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.label1.Location = new System.Drawing.Point(12, 246);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "PandAir 2018";
            // 
            // LogIn_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(561, 268);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.MakeOrder);
            this.Controls.Add(this.SignUp);
            this.Controls.Add(this.ForgotPassword);
            this.Controls.Add(this.Authorization);
            this.Controls.Add(this.PassWLabel);
            this.Controls.Add(this.LogLabel);
            this.Controls.Add(this.Password);
            this.Controls.Add(this.Login);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "LogIn_Form";
            this.ShowIcon = false;
            this.Text = "Авторизация";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Login;
        private System.Windows.Forms.TextBox Password;
        private System.Windows.Forms.Label LogLabel;
        private System.Windows.Forms.Label PassWLabel;
        private System.Windows.Forms.Button Authorization;
        private System.Windows.Forms.LinkLabel ForgotPassword;
        private System.Windows.Forms.LinkLabel SignUp;
        private System.Windows.Forms.Button MakeOrder;
        private System.Windows.Forms.Label label1;
    }
}